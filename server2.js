var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql = require('mysql');
  
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
 
// Default route
app.get('/', function (req, res) {
    return res.send({ error: true, message: 'Hallo' })
});
// Konfigurasi koneksi
var dbConn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'go_cicd'
});

// Koneksi ke database
dbConn.connect(); 

// Menampilkan data all user
app.get('/users', function (req, res) {
    dbConn.query('SELECT * FROM identitas', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'List data users.' });
    });
});


// Set port
app.listen(3050, function () {
    console.log('Node app is running on port 3050');
});

module.exports = app;